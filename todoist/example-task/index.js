export default async (browser, inputParameters) => {
  const page = await browser.newPage();

  await page.goto(inputParameters.testTargetUrl);

  const title = await page.title();

  console.log('Title', title);

  return title.toLocaleUpperCase();
}
