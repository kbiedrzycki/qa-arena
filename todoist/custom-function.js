exports.run = async function (browser, inputParameters) {
  const page = await browser.newPage();

  await page.goto(inputParameters.testTargetUrl);

  await page.click('.add-task__text');
  await (await page.$('.add-task__content')).type(inputParameters.taskTitle);
  await page.click('.add-task__submit');
  await page.waitFor(() => !document.querySelector('.add-task__content'));

  return { created: 'ok', taskTitle: inputParameters.taskTitle };
};
